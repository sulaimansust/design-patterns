package creational.abstractfactory;

import creational.abstractfactory.model.ComputerAbstractFactory;
import creational.factory.model.Computer;

public class ComputerFactory {

    public static Computer getComputer(ComputerAbstractFactory factory){
        return factory.getComputer();
    }

}
