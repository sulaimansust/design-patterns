package creational.abstractfactory;

import creational.abstractfactory.model.PCFactory;
import creational.abstractfactory.model.ServerFactory;
import creational.factory.model.Computer;

public class TestAbstractFactoryPattern {

    public static void main(String[] args) {
        testAbstractFactoryPatterns();
    }
    private static void testAbstractFactoryPatterns(){
        Computer pc = ComputerFactory.getComputer(new PCFactory("2 GB","500 GB","2.4 GHz"));
        Computer server = ComputerFactory.getComputer(new ServerFactory("16 GB","2 TB", "4.0 GHz"));
        System.out.println("AbstractFactory PC Config: "+pc);
        System.out.println("AbstractFactory Server Config: "+server);
    }
}
