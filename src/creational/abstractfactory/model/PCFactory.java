package creational.abstractfactory.model;

import creational.factory.model.Computer;
import creational.factory.model.PC;

public class PCFactory implements ComputerAbstractFactory {

    private String ram,hdd,cpu;

    public PCFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computer getComputer() {
        return new PC(ram,hdd,cpu);
    }
}
