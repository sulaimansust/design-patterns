package creational.abstractfactory.model;

import creational.factory.model.Computer;

public interface ComputerAbstractFactory {

    public Computer getComputer();
}
