package creational.abstractfactory.model;

import creational.factory.model.Computer;
import creational.factory.model.Server;

public class ServerFactory implements ComputerAbstractFactory {

    private String ram,hdd,cpu;

    public ServerFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computer getComputer() {
        return new Server(ram,hdd,cpu);
    }
}
