package creational.prototypepattern;

import java.util.List;

public class PrototypePatternTest {

    public static void main(String[] args) throws CloneNotSupportedException {
        Employees emps = new Employees();
        emps.loadData();

        Employees empsNew = (Employees) emps.clone();
        Employees empsNew1 = (Employees) emps.clone();

        List<String> list = emps.getEmpList();
        list.add("john");

        List<String> list1 = empsNew1.getEmpList();
        list1.add("mama");

        System.out.println(emps.getEmpList());
        System.out.println(empsNew.getEmpList());
        System.out.println(empsNew1.getEmpList());

    }
}
