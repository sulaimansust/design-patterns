package creational.factory;

import creational.factory.model.Computer;

public class TestFactory {
    public static void main(String[] args) {
        Computer pc = ComputerFactoryOnly.getComputer("pc","2 GB","500 GB", "3.2 GHz");
        Computer server = ComputerFactoryOnly.getComputer("server","16 GB","5 TB", "4.0 GHz");
        System.out.println("Factory PC Config: "+pc.toString());
        System.out.println("Factory Server Config: "+server);
    }
}
