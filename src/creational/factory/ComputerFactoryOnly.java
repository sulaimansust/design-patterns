package creational.factory;

import creational.factory.model.Computer;
import creational.factory.model.PC;
import creational.factory.model.Server;

public class ComputerFactoryOnly {

    public static Computer getComputer(String type, String ram, String hdd, String cpu) {
        if ("PC".equalsIgnoreCase(type))
            return new PC(ram, hdd, cpu);
        else if ("Server".equalsIgnoreCase(type))
            return new Server(ram, hdd, cpu);
        else return null;
    }
}
