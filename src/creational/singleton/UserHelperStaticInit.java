package creational.singleton;


/*
* same as eager unit always loaded but has error handling capabilities
*
* */

public class UserHelperStaticInit {
    private static UserHelperStaticInit instance;

    private UserHelperStaticInit() {

    }

    static {
        try {
            instance = new UserHelperStaticInit();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public static UserHelperStaticInit getInstance() {
        return instance;
    }

}
