package creational.singleton;

/*
* No problem like eager init and static initialization
* good but slow for synchronized
* */

public class UserHelperLazyAndThreadSafe {


    private static UserHelperLazyAndThreadSafe userHelper;

    private String name;
    private int age;

    private UserHelperLazyAndThreadSafe() {

    }

    //Single locking
    public static synchronized UserHelperLazyAndThreadSafe getInstance() {
        if (userHelper == null) {
            userHelper = new UserHelperLazyAndThreadSafe();
        }
        return userHelper;
    }

    //
    public static UserHelperLazyAndThreadSafe getInstanceUsingDoubleLocking() {
        if (userHelper == null) {
            synchronized (UserHelperLazyAndThreadSafe.class) {
                if (userHelper == null) {

                    userHelper = new UserHelperLazyAndThreadSafe();
                }
            }
        }
        return userHelper;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void destroy() {
        this.name = null;
        this.age = 0;
        userHelper = null;
    }
}
