package creational.singleton;
/*
*
* problem: always loaded though never used
* */
public class UserHelperEagerInit {

    private static final UserHelperEagerInit instance = new UserHelperEagerInit();

    private UserHelperEagerInit() {

    }

    public static UserHelperEagerInit getInstance() {
        return instance;
    }
}
