package creational.singleton;


/*
*
* This one is better ehile multiple thread access a singleton simultaneously
*
* */

public class UserHelperBillPughModel {


    private UserHelperBillPughModel() {

    }

    private static class SingletonHelper {
        private static final UserHelperBillPughModel instance = new UserHelperBillPughModel();
    }

    public static UserHelperBillPughModel getInstance() {
        return SingletonHelper.instance;
    }


}
