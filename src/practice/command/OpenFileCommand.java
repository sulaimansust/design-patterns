package practice.command;

import practice.filesystem.FileSystemReceiver;

public class OpenFileCommand implements Command{

    private FileSystemReceiver fileSystem;

    public OpenFileCommand(FileSystemReceiver fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public void execute() {
        this.fileSystem.openFile();
    }
}
