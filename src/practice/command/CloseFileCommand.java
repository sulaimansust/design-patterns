package practice.command;

import practice.filesystem.FileSystemReceiver;

public class CloseFileCommand implements Command{

    private FileSystemReceiver fileSystem;

    public CloseFileCommand(FileSystemReceiver fileSystem) {
        this.fileSystem = fileSystem;
    }


    @Override
    public void execute() {
        this.fileSystem.closeFile();
    }
}
