package practice;

import practice.filesystem.FileSystemReceiver;
import practice.filesystem.MacFileSystemReceiver;
import practice.filesystem.UnixFileSystemReceiver;
import practice.filesystem.WindowsFileSystemReceiver;

public class FileSystemReceiverUtil {
    public static FileSystemReceiver getUnderlyingFileSystem(){
        String osName = System.getProperty("os.name");
        System.out.println(osName);
        if (osName.contains("Windows")){
            return new WindowsFileSystemReceiver();
        } else if (osName.contains("Mac")){
            return new MacFileSystemReceiver();
        } else {
            return new UnixFileSystemReceiver();
        }
    }
}
