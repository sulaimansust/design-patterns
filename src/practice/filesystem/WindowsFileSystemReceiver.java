package practice.filesystem;

public class WindowsFileSystemReceiver implements FileSystemReceiver {
    @Override
    public void openFile() {
        System.out.println("Opening file in windows file system");
    }

    @Override
    public void writeFile() {
        System.out.println("Writing file in windows file system");
    }

    @Override
    public void closeFile() {
        System.out.println("Closing file in windows file system");
    }
}
