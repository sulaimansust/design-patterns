package practice.filesystem;

public interface FileSystemReceiver {
    void openFile();

    void writeFile();

    void closeFile();
}
