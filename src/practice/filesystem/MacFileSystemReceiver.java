package practice.filesystem;


public class MacFileSystemReceiver implements FileSystemReceiver {

    @Override
    public void openFile() {
        System.out.println("Opening file in mac os");
    }

    @Override
    public void writeFile() {
        System.out.println("writing file in mac os");
    }

    @Override
    public void closeFile() {
        System.out.println("Closing file in mac os");
    }
}
