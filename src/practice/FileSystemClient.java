package practice;

import practice.command.OpenFileCommand;
import practice.filesystem.FileSystemReceiver;

public class FileSystemClient {

    public static void main(String[] args) {
        FileSystemReceiver fileSystemReceiver = FileSystemReceiverUtil.getUnderlyingFileSystem();


        //Creating command and associating with receiver

        OpenFileCommand openFileCommand = new OpenFileCommand(fileSystemReceiver);

        FileInvoker fileInvoker = new FileInvoker(openFileCommand);

        fileInvoker.execute();

    }
}
