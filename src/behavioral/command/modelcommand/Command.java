package behavioral.command.modelcommand;

public interface Command {
    void execute();
}
