package behavioral.command.modelreceiver;

public class WindowsFileSystemReceiver implements FileSystemReceiver {
    @Override
    public void openFile() {
        System.out.println("openFile  in Windows OS");

    }

    @Override
    public void writeFile() {
        System.out.println("writeFile  in Windows OS");

    }

    @Override
    public void closeFile() {
        System.out.println("closeFile  in Windows OS");

    }
}
