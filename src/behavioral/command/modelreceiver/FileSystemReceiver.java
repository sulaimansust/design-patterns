package behavioral.command.modelreceiver;

public interface FileSystemReceiver {

    void openFile();
    void writeFile();
    void closeFile();


}
