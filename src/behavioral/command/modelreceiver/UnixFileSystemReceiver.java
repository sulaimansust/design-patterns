package behavioral.command.modelreceiver;

public class UnixFileSystemReceiver implements FileSystemReceiver {


    @Override
    public void openFile() {
        System.out.println("openFile  in unix OS");
    }

    @Override
    public void writeFile() {
        System.out.println("writeFile  in unix OS");

    }

    @Override
    public void closeFile() {
        System.out.println("closeFile  in unix OS");

    }
}
