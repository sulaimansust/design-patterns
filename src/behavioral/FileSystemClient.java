package behavioral;

import behavioral.command.FileInvoker;
import behavioral.command.modelcommand.CloseFileCommand;
import behavioral.command.modelcommand.OpenFileCommand;
import behavioral.command.modelcommand.WriteFileCommand;
import behavioral.command.modelreceiver.FileSystemReceiver;
import behavioral.command.utils.FileSystemReceiverUtil;

public class FileSystemClient {
    public static void main(String[] args) {
        FileSystemReceiver fileSystemReceiver = FileSystemReceiverUtil.getUnderlyingFileSystem();
        OpenFileCommand openFileCommand = new OpenFileCommand(fileSystemReceiver);

        FileInvoker fileInvoker = new FileInvoker(openFileCommand);
        fileInvoker.execute();

        WriteFileCommand writeFileCommand = new WriteFileCommand(fileSystemReceiver);
        fileInvoker = new FileInvoker(writeFileCommand);
        fileInvoker.execute();

        CloseFileCommand closeFileCommand = new CloseFileCommand(fileSystemReceiver);
        fileInvoker = new FileInvoker(closeFileCommand);
        fileInvoker.execute();

    }

}
